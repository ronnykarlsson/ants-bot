﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ants
{
    public struct Path
    {
        public Direction Direction;
        public Location Source;
        public Location Location;
        public int Distance;
        public List<Location> VisitedCollection;
    }
}
