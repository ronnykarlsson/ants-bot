﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Ants
{
    internal class Influence
    {
        private static Influence instance;

        private IGameState state;
        private MyBot bot;

        public bool aggressive = false;

        public float aggressiveness = 0.0f;

        private int[,] lastSeen;
        private int[,] attackMap;
        private int[,] assistMap;
        public int[,] strategyMap;
        private int[,] killMap;
        private float[,] explorationMap;
        private float[,] buffer;

        private const int strategyMapRange = 10;

        private const int explorationWeight = 100;
        private const float explorationDecay = 0.20f;
        private const float explorationPropagation = 0.25f;
        private const float enemyHillWeight = 10000000;
        private const float enemyAntWeight = 10000000;
        private const float defendHillWeight = 10000000;
        private const int hillDefenderAnts = 2;
        private int attackDistance;

        private int recursionNumber = 0;

        Dictionary<Direction, double> directionsDictionary;
        List<Direction> directionsList;

        public Influence(IGameState state, MyBot bot)
        {
            this.state = state;
            this.bot = bot;

            instance = this;

            directionsDictionary = new Dictionary<Direction, double>();
            directionsList = new List<Direction>();
            lastSeen = new int[state.Height, state.Width];
            attackMap = new int[state.Height, state.Width];
            assistMap = new int[state.Height, state.Width];
            strategyMap = new int[state.Height, state.Width];
            killMap = new int[state.Height, state.Width];
            explorationMap = new float[state.Height, state.Width];
            buffer = new float[state.Height, state.Width];

            attackDistance = state.ViewRadius2;

            for (int row = 0; row < state.Height; row++)
            {
                for (int col = 0; col < state.Width; col++)
                {
                    lastSeen[row, col] = 20000; // Initialize lastSeen
                }
            }
        }

        public static Influence GetInstance()
        {
            return instance;
        }

        public float GetExplorationValue(int row, int col)
        {
            return explorationMap[row, col];
        }

        public void Update()
        {
            UpdateLastSeen();

            foreach (Location enemyHill in state.EnemyHills)
                explorationMap[enemyHill.Row, enemyHill.Col] = enemyHillWeight;

            UpdateAnts(); // values for attackMap and strategyMap are set
            UpdateKillMap(); // attackMap & strategyMap => killMap
            UpdateExploration();
        }

        public List<Direction> GetDirection(Location ant)
        {
            Location newLocation;

            directionsDictionary.Clear();
            directionsList.Clear();

            foreach (Direction direction in Ants.Aim.Keys)
            {
                newLocation = state.GetDestination(ant, direction);
                if (aggressive || strategyMap[newLocation.Row, newLocation.Col] == 0) // In case our attack-logic missed something
                {
                    double newValue = explorationMap[newLocation.Row, newLocation.Col];
                    if (newValue > 0) directionsDictionary.Add(direction, newValue);
                }
            }

            foreach (KeyValuePair<Direction, double> direction in directionsDictionary.OrderByDescending(dir => dir.Value))
            {
                directionsList.Add(direction.Key);
            }

            return directionsList;
        }

        public void Attack()
        {
            recursionNumber = 0;
            for (int i = 0; i < state.MyAnts.Count; i++)
            {
                int antCount = state.MyAnts.Count;
                AttackKillMap(state.MyAnts[i], i);
                if (antCount != state.MyAnts.Count) i--;
            }
        }

        public void OrderAntHoldPosition(Location ant, int antIndex)
        {
            state.MyAnts.RemoveAt(antIndex);
            state.SetUnoccupied(ant);
            bot.AddOrder(ant);
            assistMap[ant.Row, ant.Col]++;
#if VISUALIZER_STRATEGY
            Visualizer.OutputTile(ant.Row, ant.Col, new Color(0, 0, 255, 0.7f));
#endif
        }

        public void OrderAntMove(Location ant, int antIndex, Direction direction)
        {
            state.MyAnts.RemoveAt(antIndex);
            state.SetUnoccupied(ant);
            bot.MoveAnt(ant, direction);
            Location newLocation = state.GetDestination(ant, direction);
            assistMap[newLocation.Row, newLocation.Col]++;
#if VISUALIZER_STRATEGY
            Visualizer.OutputTile(ant.Row, ant.Col, new Color(255, 0, 0, 0.7f));
#endif
        }

        /// <summary>
        /// Check if ant can move in a specific direction
        /// </summary>
        /// <param name="ant">Location of ant</param>
        /// <param name="direction">Direction to move</param>
        /// <returns>False if location isn't an ant, true if ant can move without errors.</returns>
        public bool CanMove(Location ant, Direction direction)
        {
            Location destination = state.GetDestination(ant, direction);

            // Verify that this actually is an ant
            int index = state.MyAnts.BinarySearch(ant);
            if (index < 0)
            {
                return false;
            }

            if (state.GetIsUnoccupied(destination) && assistMap[destination.Row, destination.Col] == 0)
            {
                return true;
            }

            return false;
        }

        private void AttackKillMap(Location ant, int antIndex)
        {
            // In case we get into a bad loop
            if (recursionNumber > 10)
            {
                return;
            }

            Direction bestDirection = (Direction)(-1);
            float bestValue = killMap[ant.Row, ant.Col]; // weight for staying in the tile

            if (strategyMap[ant.Row, ant.Col] <= 0)
            {
                bestDirection = (Direction)(-10);
            }

            Location checkLocation = new Location();
            foreach (Direction direction in Ants.Aim.Keys)
            {
                checkLocation = state.GetDestination(ant, direction);

                if (!state.GetIsPassable(checkLocation)) continue;

                // is this new weight better?
                float newValue = killMap[checkLocation.Row, checkLocation.Col];
                if ((bestDirection != (Direction)(-10) || strategyMap[checkLocation.Row, checkLocation.Col] > 0) && newValue > bestValue)
                {
                    bestValue = newValue;
                    bestDirection = direction;
                }
            }

            // Move ant to the new tile
            if (bestDirection >= (Direction)0)
            {
                if (!CanMove(ant, bestDirection)) // Ant in the way? ask him to move
                {
                    Location otherAnt = state.GetDestination(ant, bestDirection);
                    antIndex = state.MyAnts.BinarySearch(otherAnt);
                    recursionNumber++;
                    if (antIndex >= 0) AttackKillMap(otherAnt, antIndex);
                    recursionNumber--;
                    antIndex = state.MyAnts.BinarySearch(ant);
                }
                if (CanMove(ant, bestDirection) && antIndex >= 0) // Move second ant, begin recursion here
                {
                    OrderAntMove(ant, antIndex, bestDirection);
                    killMap[checkLocation.Row, checkLocation.Col] -= 20;
                }
                else // Hold position if we can't move
                {
                    OrderAntHoldPosition(ant, antIndex);
                    killMap[checkLocation.Row, checkLocation.Col] -= 20;
                }
            }
            if (bestDirection == (Direction)(-1)) // Direction -1  =>  Hold position is best for us
            {
                OrderAntHoldPosition(ant, antIndex);
            }
        }

        /// <summary>
        /// Read through all ants and update strategyMap and attackMap
        /// </summary>
        public void UpdateAnts()
        {
            Location checkLocation;

            for (int row = 0; row < state.Height; row++)
            {
                checkLocation.Row = row;
                for (int col = 0; col < state.Width; col++)
                {
                    checkLocation.Col = col;
                    int strategyCount = 0;
                    int attackCount = 0;
                    int assistanceCount = 0;

                    int minDistance = 100;

                    foreach (Location ant in state.EnemyAnts)
                    {
                        int newDistance2 = state.GetDistance2(checkLocation, ant);
                        minDistance = Math.Min(minDistance, newDistance2);
                        if (newDistance2 <= strategyMapRange)
                        {
                            strategyCount++;
                        }
                    }

                    if (minDistance <= 17)
                    {
                        foreach (Location ant in state.MyAnts)
                        {
                            int newDistance2 = state.GetDistance2(checkLocation, ant);
                            if (newDistance2 <= 17) // Attack radius + 2 tiles, to indicate that other ants in the field should wait rather than running away
                            {
                                assistanceCount++;
                                if (newDistance2 <= 10) // Attack radius + 1 tile, to indicate how to organize attacks
                                {
                                    attackCount++;
                                }
                            }
                        }
                    }

                    attackMap[row, col] = attackCount; // Save friendly count
                    strategyMap[row, col] = strategyCount; // Save hostile count
                    assistMap[row, col] = 0; // Use this loop to also zero the assistMap each turn
                }
            }
        }

        /// <summary>
        /// Make a killMap for strategic moves from strategyMap and killMap
        /// </summary>
        private void UpdateKillMap()
        {
            int distance = 0;

            foreach (Location ant in state.MyAnts)
            {
                Location bestAnt = new Location(-1, -1);

                int bestDistance = 0;

                int hillDefender = 0;
                if (aggressive) hillDefender = 2;
                else
                {
                    foreach (Location hill in state.MyHills)
                    {
                        if (state.GetDistance2(ant, hill) <= state.ViewRadius2)
                        {
                            hillDefender = 2;
                            break;
                        }
                    }
                }

                if (bot.turnNumber > 160) hillDefender += 2; // Get a little more offensive at kill-squares after 160 turns

                int strategyCount = strategyMap[ant.Row, ant.Col]; // No need to iterate more enemies than necessary

                if (strategyCount > 0)
                {
                    foreach (Location enemyAnt in state.EnemyAnts)
                    {
                        distance = state.GetDistance2(enemyAnt, ant);
                        if (distance <= 10)
                        {
                            if (bestAnt.Row == -1 || attackMap[enemyAnt.Row, enemyAnt.Col] > attackMap[bestAnt.Row, bestAnt.Col])
                            {
                                bestAnt = enemyAnt; // See if we can kill him
                                bestDistance = distance;
                            }
                            if (--strategyCount <= 0) break; // Done iterating?
                        }
                    }
                }

                if (bestAnt.Row == -1) // no ant found
                {
                    killMap[ant.Row, ant.Col] = 2; // SAFE
                }
                else if (attackMap[bestAnt.Row, bestAnt.Col] < strategyMap[ant.Row, ant.Col])
                {
                    killMap[ant.Row, ant.Col] = -2 * (strategyMap[ant.Row, ant.Col] - attackMap[bestAnt.Row, bestAnt.Col]); // DIE
                }
                else if (attackMap[bestAnt.Row, bestAnt.Col] == strategyMap[ant.Row, ant.Col])
                {
                    killMap[ant.Row, ant.Col] = 1 + hillDefender; // KILL
                    if (hillDefender > 0) killMap[ant.Row, ant.Col] += (10 - bestDistance);
                }
                else
                {
                    killMap[ant.Row, ant.Col] = 6 + 2 * attackMap[bestAnt.Row, bestAnt.Col] + (10 - bestDistance); // BETTER KILL
                }

                foreach (Direction direction in Ants.Aim.Keys) // Check each direction
                {
                    Location checkLocation = state.GetDestination(ant, direction);

                    if (!state.GetIsPassable(checkLocation)) continue; // Don't bother with tiles we can't move to

                    bestAnt.Row = -1;
                    strategyCount = strategyMap[checkLocation.Row, checkLocation.Col]; // No need to iterate more enemies than necessary

                    if (strategyCount > 0)
                    {
                        foreach (Location enemyAnt in state.EnemyAnts)
                        {
                            distance = state.GetDistance2(enemyAnt, checkLocation);
                            if (distance <= 10)
                            {
                                if (bestAnt.Row == -1 || attackMap[enemyAnt.Row, enemyAnt.Col] > attackMap[bestAnt.Row, bestAnt.Col])
                                {
                                    bestAnt = enemyAnt;
                                    bestDistance = distance;
                                }
                                if (--strategyCount <= 0) break; // Done iterating
                            }
                        }
                    }

                    if (bestAnt.Row == -1) // no ant found
                    {
                        killMap[checkLocation.Row, checkLocation.Col] = 2; // SAFE
                    }
                    else if (attackMap[bestAnt.Row, bestAnt.Col] < strategyMap[checkLocation.Row, checkLocation.Col])
                    {
                        killMap[checkLocation.Row, checkLocation.Col] = -2 * (strategyMap[checkLocation.Row, checkLocation.Col] - attackMap[bestAnt.Row, bestAnt.Col]); // DIE
                    }
                    else if (attackMap[bestAnt.Row, bestAnt.Col] == strategyMap[checkLocation.Row, checkLocation.Col])
                    {
                        killMap[checkLocation.Row, checkLocation.Col] = 1 + hillDefender; // KILL
                        if (hillDefender > 0) killMap[checkLocation.Row, checkLocation.Col] += (10 - bestDistance);
                    }
                    else
                    {
                        killMap[checkLocation.Row, checkLocation.Col] = 6 + 2 * attackMap[bestAnt.Row, bestAnt.Col] + (10 - bestDistance); // BETTER KILL
                    }
                }
            }
        }

        /// <summary>
        /// Tell whether there is an ant assisting at <paramref name="location"/>.
        /// </summary>
        /// <param name="location">Location to check</param>
        /// <returns>True if there is an ant, false otherwise.</returns>
        public bool HasAssist(Location location)
        {
            if (assistMap[location.Row, location.Col] > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Update explorationMap to spread ants
        /// </summary>
        private void UpdateExploration()
        {
            Location northLocation, eastLocation, southLocation, westLocation;
            Location currentLocation;

            float[,] isOccupied = state.GetIsOccupied();

            for (int i = 0; i < bot.searchDistance; i++)
            {
                // Update explorationMap to get weights
                for (int row = 0; row < state.Height; row++)
                {
                    currentLocation.Row = row;
                    northLocation.Row = (row == 0 ? state.Height - 1 : row - 1);
                    eastLocation.Row = row;
                    southLocation.Row = (row == state.Height - 1 ? 0 : row + 1);
                    westLocation.Row = row;

                    for (int col = 0; col < state.Width; col++)
                    {
                        currentLocation.Col = col;

                        northLocation.Col = col;
                        eastLocation.Col = (col == state.Width - 1 ? 0 : col + 1);
                        southLocation.Col = col;
                        westLocation.Col = (col == 0 ? state.Width - 1 : col - 1);


                        // Diffusion formula
                        float value = explorationMap[northLocation.Row, northLocation.Col] + explorationMap[eastLocation.Row, eastLocation.Col]
                            + explorationMap[southLocation.Row, southLocation.Col] + explorationMap[westLocation.Row, westLocation.Col];
                        value = (explorationMap[row, col] + explorationPropagation * (value - 4 * explorationMap[row, col])) * isOccupied[row, col];

                        buffer[row, col] = value; // Save the new value
                    }
                }

                // Swap references, save the old map as a buffer so we don't need to allocate new memory
                float[,] tempBuffer = explorationMap;
                explorationMap = buffer;
                buffer = tempBuffer;
            }
        }

        /// <summary>
        /// Update hidden tiles and initiailize explorationMap
        /// </summary>
        private void UpdateLastSeen()
        {
            Location checkLocation;

            // Update last seen
            for (int row = 0; row < state.Height; row++)
            {
                checkLocation.Row = row;
                for (int col = 0; col < state.Width; col++)
                {
                    checkLocation.Col = col;
                    if (state.EnemyHills.Contains(checkLocation))
                    {
                        explorationMap[row, col] = enemyHillWeight; // This will attract our ants towards enemy hills
                    }
                    else
                    {
                        if (!state.GetIsPassable(checkLocation)) // We don't need to see the water-tiles, just assume it stays being water
                        {
                            lastSeen[row, col] = 0;
                        }
                        else
                        {
                            lastSeen[row, col]++;
                            foreach (Location ant in state.MyAnts)
                            {
                                float distance2 = state.GetDistance2(ant, checkLocation);
                                if (distance2 <= state.ViewRadius2) // check if ant can see the current square
                                {
                                    lastSeen[row, col] = 0;

                                    // Decay the exploration-values for visible tiles
                                    float value = explorationMap[row, col];
                                    value = value * explorationDecay;
                                    explorationMap[row, col] = value;
                                    break;
                                }
                            }
                        }

                        if (lastSeen[row, col] > 0) explorationMap[row, col] = 100 + lastSeen[row, col] * 30; // Initialize explorationMap here
                    }
                }
            }
        }
    }
}
