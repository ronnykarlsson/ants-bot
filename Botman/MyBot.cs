using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;


namespace Ants
{

    internal class MyBot : Bot
    {

        private List<Location> orders = new List<Location>();
        private const int attackDistance = 10;
        private const int foodDistance = 12;

        private Influence influence;

        public int searchDistance = 10;
        private int maxSearchDistance;

        public int turnNumber = 0;

        IGameState state;

        // DoTurn is run once per turn
        public override void DoTurn(IGameState state)
        {
            orders.Clear();
            
            this.state = state;

            if (turnNumber++ == 0)
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                //Debugger.Launch();

                // Load influence map for exploration/combat
                influence = new Influence(state, this);

                maxSearchDistance = state.Width + state.Height;
            }
            else
            {
                // Adjust behavior
                if (state.MyAnts.Count*5 < maxSearchDistance*4)
                {
                    influence.aggressive = false;
                    //influence.aggressiveness = 0.05f;
                }
                else if (state.MyAnts.Count > maxSearchDistance)
                {
                    influence.aggressive = true;
                    //influence.aggressiveness = 1.0f;
                }
                else // Gather up ants before being aggressive
                {
                    influence.aggressive = false;
                    //influence.aggressiveness = 1.0f;
                }
            }

            influence.Update();

            // Battle enemy-ants
            influence.Attack();

            // Attack enemy hills in sight
            AttackHills(state);

            // Search nearest ant for each food tile
            FindFood(state);

            // Exploration
            Exploration(state);

            if (state.TimeRemaining < 100)
            {
                searchDistance = (searchDistance <= 10 ? searchDistance = 5 : searchDistance - 7);
            }
            else
            {
                searchDistance = (searchDistance >= maxSearchDistance ? searchDistance : searchDistance + 1);
            }
            //searchDistance = 30;
        }

        public bool MoveAnt(Location ant, Direction direction)
        {
            Location newLocation = state.GetDestination(ant, direction);
            if (AddOrder(newLocation))
            {
                IssueOrder(ant, direction);
                return true;
            }
            else return false;
        }

        public bool AddOrder(Location order)
        {
            int index = orders.BinarySearch(order);
            if (index >= 0) return false; // Don't add orders that already exist

            orders.Insert(~index, order);
            return true;
        }

        private void FindFood(IGameState state)
        {
            SearchLocations(state, state.FoodTiles, true, path =>
            {
                if (path.Distance >= foodDistance)
                {
                    return true;
                }

                int antIndex = state.MyAnts.BinarySearch(path.Location);
                if (antIndex >= 0)
                {
                    if (path.Distance == 1)
                    {
                        // We're already next to the food, just stay put and it'll be ours
                        AddOrder(path.Location);
                        state.SetUnoccupied(state.MyAnts[antIndex]);
                        state.MyAnts.RemoveAt(antIndex);
                    }
                    else
                    {
                        // Path found its destination, move the ant
                        Direction inverseDirection = state.GetInverseDirection(path.Direction);
                        Location newOrder = state.GetDestination(path.Location, inverseDirection);
                        if (state.GetIsUnoccupied(newOrder))
                        {
                            if (influence.strategyMap[newOrder.Row, newOrder.Col] > 0) return true; // don't walk into combat

                            if (AddOrder(newOrder))
                            {
                                IssueOrder(path.Location, inverseDirection);
                                state.MyAnts.RemoveAt(antIndex);
                                return true;
                            }
                        }
                    }
                }
                return false; // Not found, keep searching
            });
        }

        private void AttackHills(IGameState state)
        {
            SearchLocations(state, state.EnemyHills, true, path =>
            {
                if (path.Distance >= attackDistance) return true; // Send the ants who are close enough to attack

                int antIndex = state.MyAnts.BinarySearch(path.Location);
                if (antIndex >= 0)
                {
                    Location newLocation = state.GetDestination(path.Location, state.GetInverseDirection(path.Direction));
                    if (state.GetIsUnoccupied(newLocation) && influence.strategyMap[newLocation.Row, newLocation.Col] <= 1)
                    {
                        // Send ant towards hill
                        if (AddOrder(newLocation))
                        {
                            IssueOrder(path.Location, state.GetInverseDirection(path.Direction));
                            state.SetUnoccupied(path.Location);
                            state.MyAnts.Remove(path.Location);
                        }
                    }
                }

                return false;
            });
        }

        private void Exploration(IGameState state)
        {
            foreach (Location ant in state.MyAnts)
            {
                List<Direction> directions = influence.GetDirection(ant);
                foreach (Direction direction in directions)
                {
                    Location newLocation = state.GetDestination(ant, direction);
                    if (state.GetIsUnoccupied(newLocation) && !influence.HasAssist(newLocation))
                    {
                        // Send ant for exploration
                        if (AddOrder(newLocation))
                        {
                            IssueOrder(ant, direction);
                            break;
                        }
                    }
                }
            }
        }

        public void SearchLocation(IGameState state, Location location, bool updateDirection, Func<Path, bool> CheckTarget)
        {
            List<Location> locations = new List<Location>() { location };
            SearchLocations(state, locations, updateDirection, path => CheckTarget(path));
        }

        public void SearchLocations(IGameState state, IList<Location> locations, bool updateDirection, Func<Path, bool> CheckTarget)
        {
            List<Path> frontier = new List<Path>();
            List<Location> IsDoneCollection = new List<Location>();

            // Add locations to frontier
            Path path;
            path.Direction = Direction.North;
            foreach (Location location in locations)
            {
                path.Source = location;
                path.Location = location;
                path.Distance = 0;
                path.VisitedCollection = new List<Location>();
                frontier.Add(path);
            }

            int frontierCount = frontier.Count;
            while (frontierCount > 0)
            {
                for (int i = 0; i < frontierCount; i++)
                {
                    Path currentPath = frontier[0];
                    frontier.RemoveAt(0);

                    // Make sure source didn't already find its destination through another route
                    if (IsDoneCollection.Contains(currentPath.Source)) continue;

                    // Try expand currentPath in each direction
                    foreach (Direction direction in Ants.Aim.Keys)
                    {
                        Location newLoc = state.GetDestination(currentPath.Location, direction);
                        if (state.GetIsPassable(newLoc))
                        {
                            int visitedIndex = currentPath.VisitedCollection.BinarySearch(newLoc);
                            if (visitedIndex < 0)
                            {
                                // Make a new path for the expanded location
                                Path newPath;
                                if (updateDirection) newPath.Direction = direction;
                                else
                                {
                                    // If this is the first iteration, we still gotta set direction the first time
                                    if (currentPath.Distance == 0)
                                    {
                                        newPath.Direction = direction;
                                    }
                                    else // But otherwise we just keep the first direction that was set, don't update it
                                    {
                                        newPath.Direction = currentPath.Direction;
                                    }
                                }
                                newPath.Source = currentPath.Source;
                                newPath.VisitedCollection = currentPath.VisitedCollection;
                                newPath.Location = newLoc;
                                newPath.Distance = currentPath.Distance + 1;

                                // Before adding path to frontier, check if it reached the destination
                                if (CheckTarget(newPath))
                                {
                                    IsDoneCollection.Add(currentPath.Source);
                                    break;
                                }
                                else
                                {
                                    frontier.Add(newPath);
                                    // Mark location as visited right away, no need to check for other paths leading to the same location
                                    newPath.VisitedCollection.Insert(~visitedIndex, newLoc);
                                }
                            }
                        }
                    }
                }
                frontierCount = frontier.Count;
            }
        }

        protected new void IssueOrder(Location loc, Direction direction)
        {
            base.IssueOrder(loc, direction);
            state.SetUnoccupied(loc);
        }

        public static void Main(string[] args)
        {
            new Ants().PlayGame(new MyBot());
        }

    }

}
