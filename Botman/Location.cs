using System;
using System.Collections.Generic;

namespace Ants
{

    public struct Location : IEquatable<Location>, IComparable<Location>
    {
        /// <summary>
        /// Gets the row of this location.
        /// </summary>
        public int Row;

        /// <summary>
        /// Gets the column of this location.
        /// </summary>
        public int Col;

        public Location(int row, int col)
        {
            this.Row = row;
            this.Col = col;
        }

        public override bool Equals(object obj)
        {
            throw new NotImplementedException();
            //if (ReferenceEquals(null, obj))
            //    return false;
            //if (ReferenceEquals(this, obj))
            //    return true;
            //if (obj.GetType() != typeof(Location))
            //    return false;

            //return Equals((Location)obj);
        }

        public override string ToString()
        {
            return this.Row + ", " + this.Col;
        }

        public bool Equals(Location other)
        {
            return other.Row == this.Row && other.Col == this.Col;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (this.Row * 397) ^ this.Col;
            }
        }

        public int CompareTo(Location other)
        {
            if (this.GetHashCode() > other.GetHashCode()) return 1;
            else if (this.GetHashCode() < other.GetHashCode()) return -1;
            return 0;
        }
    }

    //public class TeamLocation : Location, IEquatable<TeamLocation> {
    //    /// <summary>
    //    /// Gets the team of this ant.
    //    /// </summary>
    //    public int Team { get; private set; }

    //    public TeamLocation (int row, int col, int team) : base (row, col) {
    //        this.Team = team;
    //    }

    //    public bool Equals(TeamLocation other) {
    //        return base.Equals (other) && other.Team == Team;
    //    }

    //    public override int GetHashCode()
    //    {
    //        unchecked {
    //            int result = this.Col;
    //            result = (result * 397) ^ this.Row;
    //            result = (result * 397) ^ this.Team;
    //            return result;
    //        }
    //    }
    //}

    public struct Ant : IEquatable<Ant>
    {

        public int Row;
        public int Col;
        public int Team;

        public Ant(int row, int col, int team)
        {
            Row = row;
            Col = col;
            Team = team;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != typeof(Ant))
                return false;

            return Equals((Ant)obj);
        }

        public bool Equals(Ant other)
        {
            return other.Row == this.Row && other.Col == this.Col && other.Team == this.Team;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (this.Row * 397) ^ this.Col ^ this.Team;
            }
        }

    }

    public struct AntHill : IEquatable<AntHill>
    {
        public int Row;
        public int Col;
        public int Team;

        public AntHill(int row, int col, int team)
        {
            Row = row;
            Col = col;
            Team = team;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != typeof(AntHill))
                return false;

            return Equals((AntHill)obj);
        }

        public bool Equals(AntHill other)
        {
            return other.Row == this.Row && other.Col == this.Col && other.Team == this.Team;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (this.Row * 397) ^ this.Col ^ this.Team;
            }
        }
    }
}

