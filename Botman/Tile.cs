using System;

namespace Ants
{
    [Flags]
    public enum Tile
    {
        Ant = 0x01,
        Dead = 0x02,
        Land = 0x04,
        Food = 0x08,
        Water = 0x10,
        Unseen = 0x12,
        Hill = 0x14
    }
}

